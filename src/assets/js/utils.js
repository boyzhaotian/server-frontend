'use strict';

import './security.js'

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
        BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
        iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
        Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
        Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
        any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var fullHeight = function() {

    let setHeight = () => {
        let doms = document.getElementsByClassName('js-fullheight')
        for (let i = 0; i < doms.length; i++) {
            doms[i].style.height = window.innerHeight + 'px'
        }
    }
    window.onresize = () => {
        setHeight()
    }
    !isMobile.any()&&setHeight()

};

var init = () => {
    fullHeight()
}

var clone = (obj) => {
    return JSON.parse(JSON.stringify(obj))
}
var timer = null
var debounce = (fn, delay) => {
    console.log(timer);
    clearTimeout(timer)
    timer = setTimeout(() => {
        fn&&fn()
    }, delay);
}

Function.prototype.getMultiLine = function () {
    var lines = new String(this);
    lines = lines.substring(lines.indexOf("/*") + 3, lines.lastIndexOf("*/"));
    return lines;
}
var draws = {
    ghost() {
        /*
        .-')   .-. .-')               
        ( OO ). \  ( OO )              
        (_)---\_),--. ,--.   ,--.   ,--.
        /    _ | |  .'   /    \  `.'  / 
        \  :` `. |      /,  .-')     /  
         '..`''.)|     ' _)(OO  \   /   
        .-._)   \|  .   \   |   /  /\_  
        \       /|  |\   \  `-./  /.__) 
        `-----' `--' '--'    `--'      
        */
    },subZero() {
        /*
         ______     __  __     __  __    
        /\  ___\   /\ \/ /    /\ \_\ \   
        \ \___  \  \ \  _"-.  \ \____ \  
         \/\_____\  \ \_\ \_\  \/\_____\ 
          \/_____/   \/_/\/_/   \/_____/ 
        */
    }
}

export {
    isMobile,
    fullHeight,
    init,
    clone,
    debounce,
    draws
}