'use strict'

// 防止被 iframe 嵌套
if (self != top) {
  top.location = location;
}

// 重写 document.write
document.write = () => {}

