const
    FADEOUT = 200,
    STAND  = 2000,

    BGSTYLE = {
        transition: FADEOUT + 'ms',
        position: 'fixed',
        display: 'none',
        zIndex: 9999,
        bottom: 0,
        right: 0,
        left: 0,
        top: 0,
    },
    POPUPSTYLE = {
        transform: 'translate(-50%,-50%)',
        position: 'fixed',
        left: '50%',
        top: '50%',
        transition: FADEOUT + 'ms',
        textAlign: 'center',
        borderRadius: '.3em',
        backgroundColor: 'rgba(57,58,59,.5)',
        lineHeight: '2em',
        padding: '0 1em',
        fontSize: '1em',
        color: 'white',
        zIndex: '999'
    }
/** 自有方法 */
// 给dom元素添加style
function _setStyleToDom( dom, style ) {
    for (const key in style) {
        if (style.hasOwnProperty(key)) {
            dom.style[key] = style[key];
        }
    }
}
// 判断是否存在相同id的元素
function _lockExist(lockId) {
    let lockExist = !!document.getElementById(lockId)
    if (lockExist) return true
    return false
}

// 提示框方法
// 设置lockId可防止多次添加相同弹框导致重叠
function toast(msg,lockId) {

    // 生成新提示
    let popup = new MyToast(msg,lockId)
    popup.show()
    setTimeout(()=>popup.delete(), STAND );

}

function MyToast(text,lockId) {
    this.wrapper = document.createElement('div')
    this.elem = document.createElement('div')
    this.elem.innerText = text
    this.elem.id = lockId
    _setStyleToDom( this.wrapper, BGSTYLE )
    _setStyleToDom( this.elem, POPUPSTYLE )
    this.wrapper.appendChild(this.elem)
    if ( _lockExist( lockId )) {
        this.lockExist = true
    } else {
        document.body.appendChild(this.wrapper)
    }
    document.body.appendChild(this.wrapper)
    this.show = () => {
        this.wrapper.style.display = 'block'
        this.wrapper.style.opacity = 1
    }
    this.delete = () => {
        this.wrapper.style.opacity = 0
        // !this.lockExist&&setTimeout(()=>document.body.removeChild(this.wrapper), FADEOUT);
        setTimeout(()=>document.body.removeChild(this.wrapper), FADEOUT);
    }
    this.hide = () => {
        this.wrapper.style.opacity = 0
        setTimeout(()=>this.wrapper.style.display = 'none', FADEOUT);
    }
}

function MyPrompt() {
    MyToast.call(this,arguments)
    let input = document.createElement('input')
    let btn = document.createElement('button')
    this.elem.appendChild(input)
    this.elem.appendChild(btn)
}

(function() {
    // 创建一个没有实例方法的类
    var Super = function(){};
    Super.prototype = MyToast.prototype;
    //将实例作为子类的原型
    MyPrompt.prototype = new Super();
})()

export default toast