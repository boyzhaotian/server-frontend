let useLocal = true

let MYSERVER = 'zhaosky.cn'
let PROTOCAL = location.protocol
let HOSTNAME = location.hostname
let ORIGIN = PROTOCAL + '//' + HOSTNAME + ':3000'

let localHostList = [
    'localhost',
    '127.0.0.1',
    '0.0.0.0',
]

let isLocal = localHostList.indexOf(HOSTNAME) !== -1

if ( isLocal ) !useLocal && ( ORIGIN = PROTOCAL + '//' + MYSERVER + '/api' )

export default {
    origin: ORIGIN
}