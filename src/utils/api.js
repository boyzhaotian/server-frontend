import ports from '@/config/ports.js'
import server from './server'
import Axios from 'axios';

let origin = server.origin
let API = {}

Axios.defaults.withCredentials = true

// Add a request interceptor
Axios.interceptors.request.use( config => {
    // Do something before request is sent
    return config;
}, error => {
    // Do something with request error
    return Promise.reject(error);
})

// Add a response interceptor
Axios.interceptors.response.use(function (response) {
    // Do something with response data
    console.log(response);
    let data
    if ( response.status == 200 ) {
        data = response.data
    } else {
        data = {}
    }

    return data;
}, function (error) {
    // Do something with response error
    return Promise.reject(error);
});

function run(api) {
    let module = api.module === undefined ? ports.module : api.module
    let path = api instanceof Object ? api.path || '/' : api

    let url = origin
    if (module) { url += '/' + module; }
    url += path

    return ( data ) => {
        let result = new Promise( ( resolve, reject ) => {
            Axios({
                method: api.type || 'POST',
                url: url,
                data: data
            }).then( res => {
                resolve(res)
            }, error => {
                reject(error)
            })
        })
        return result
    }
}

for (const api in ports.list) {
    if (ports.list.hasOwnProperty(api)) {
        API[api] = run(ports.list[api])
    }
}

export default {
    install(Vue) {
        Object.defineProperty( Vue.prototype, 'API', { value: API } )
    }
}