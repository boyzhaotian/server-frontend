export default {
    module: 'api',
    list: {
        sendMail: '/mail',
        test: '/test'
    }
}