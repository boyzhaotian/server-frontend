import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		guide: !localStorage.getItem('guide') || localStorage.getItem('guide') === 'true',
		guideTime: 5000,
		user: localStorage.getItem('ZHAOSKY_USER'),
		socket: {
			connected: false
		},
		chat: {
			history: []
		}
	},
	getters: {
		user: state => {
			return JSON.parse(state.user)
		},
		chatHistory: state => {
			return state.chat.history
		}
	},
	mutations: {
		guide ( state, data ) {
			localStorage.setItem('guide', data)
			state.guide = data
		},
		initHistory ( state, data ) {
			state.chat.history = data
		},
		addChatHistory ( state, data ) {
			state.chat.history.push(data)
		},
		'SOCKET_CONNECTED' ( state, data ) {
			state.socket.connected = true
			console.log(data);
		},
		'SOCKET_UPDATE_USER' ( state, userdata ) {
			const dataStr = JSON.stringify(userdata)
			localStorage.setItem('ZHAOSKY_USER', dataStr)
			state.user = dataStr
		}
	},
	actions: {
		"SOCKET_INIT_HISTORY" ( context, data ) {
			context.commit('initHistory',data)
		},
		"SOCKET_NEW_HISTORY" ( context, data ) {
			context.commit('addChatHistory',data)
		},
		"SOCKET_LOGIN" ( context, data ) {
			console.log( context, data );
		},
	}
})
