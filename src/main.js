import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/css/icomoon.css'
import '@/assets/css/weui.min.css'
import api from '@/utils/api'
import VueSocketIO from 'vue-socket.io'
import server from './utils/server';
import '@/assets/js/reset'

Vue.config.productionTip = false
Vue.use(api)

// Vue.use(new VueSocketIO({
//   debug: true,
//   connection: server.origin + '/chat',
//   vuex: {
//       store,
//       actionPrefix: 'SOCKET_',
//       mutationPrefix: 'SOCKET_'
//   }
// }))

new Vue({
  router,
  store,
  render: h => h(App),
  sockets: {
    connect() {
      console.log('socket connected')
      const user = this.$store.getters.user
      this.$socket.emit('LOGIN',user&&user._id)
    },
    newUser() {

    },
    news(data) {
      console.log('this method was fired by the socket server. eg: io.emit("customEmit", data)',data)
    }
  },
}).$mount('#app')
